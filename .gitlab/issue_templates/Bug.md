## Bug Report
_Provide a summary an issue that you came across._

### Expected Behavior
_Write a description of the behavior that you was expecting._


### Current Behavior
_Write a description of the behavior that is occuring._


### Environment
_Add a description of the environment where the bug was discovered. This includes software version, operating system and etc._

#### Relevant Resources
_Share any logs, errors or screenshots that will help describe the issue._

### Reproduction Steps
_Provide a list of steps that you did, so that we can reproduce the issue._
1.
2.
3.