import os
import cv2
import imutils
import imageio
import csv

def main():

    raw_image_path = "/Users/zuri/Documents/Experiments/isthis4c/rawimages/2chair"
    processed_image_path = "/Users/zuri/Documents/Experiments/isthis4c/train/2chair"

    for image_path in os.listdir(raw_image_path):
        full_path = os.path.join(raw_image_path,image_path)
        img = cv2.imread(full_path, 1)
        if img is None:
           continue

        resized_img = imutils.resize(img, width=1280)
        train_image_full_path = os.path.join(processed_image_path, 'train_image_'+ image_path)
        imageio.imwrite(train_image_full_path, resized_img)

    with open('/Users/zuri/Documents/Experiments/isthis4c/train/2chair/2chair.csv', 'w') as csvfile:
        for image_path in os.listdir(raw_image_path):
            ## TODO: Need to add headers to csv with id, label
            filewriter = csv.writer(csvfile, delimiter=',',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)
            if image_path == '.DS_Store':
                continue
            create_row = [image_path, '2chair']
            filewriter.writerow(create_row)


if __name__ == '__main__':
    main()