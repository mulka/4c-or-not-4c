import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.utils import to_categorical
from keras.preprocessing import image
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from keras.utils import to_categorical
from tqdm import tqdm

#load training image for 4cblackhair
train = pd.read_csv('/Users/zuri/Documents/Experiments/isthis4c/train/4cblackhair/4cblackhair.csv')
processed_image_path = "/Users/zuri/Documents/Experiments/isthis4c/train/2chair"

#store 4cblackhair images in a list
train_image = []

# read training images, store em in a list, convert into a numpy array
for i in tqdm(range(train.shape[0])):
    img = image.load_img('/Users/zuri/Documents/Experiments/isthis4c/train/4cblackhair/train_image_' + train['id'][i], target_size=(28, 28, 1), color_mode='grayscale')
    img = image.img_to_array(img)
    img = img/255
    train_image.append(img)
X = np.array(train_image)

# multi-class so adding in target value in our case it is 4cblackhair label
# Y =['id', 'label'] ## issue with >>> y=train['label'].values
converting=train['label'].values
onehot = pd.get_dummies(converting)
target_labels = onehot.columns
Y = onehot.as_matrix()
y = to_categorical(Y)
print('category',y.shape)

# establish validation set
X_train, X_test, y_train, y_test = train_test_split(X,y, random_state=42, test_size=0.2)

# Define model structure >>> 2 convolutional layers, one dense hidden layer and an output layer.
model = Sequential()
model.add(Conv2D(32, kernel_size=(3, 3),activation='relu',input_shape=(28,28,1)))
model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))
model.add(Flatten())
model.add(Dense(128, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(2, activation='softmax')) ## need to make sure it matches class mode?? and switch to two

# compile model
model.compile(loss='categorical_crossentropy',optimizer='Adam',metrics=['accuracy'])

# train model
model.fit(X_train, y_train, epochs=10, validation_data=(X_test, y_test))


# read and store test images
test = pd.read_csv('/Users/zuri/Documents/Experiments/isthis4c/test/csv/test.csv')

test_image = []
for i in tqdm(range(test.shape[0])):
    # img = image.load_img('/Users/zuri/Documents/Experiments/isthis4c/'+test['id'][i].astype('str')+'.jpg', target_size=(28,28,1), grayscale=True)
    img = image.load_img('/Users/zuri/Documents/Experiments/isthis4c/test/' + test['id'][i], target_size=(28, 28, 1), color_mode='grayscale')

    img = image.img_to_array(img)
    img = img/255
    test_image.append(img)
test = np.array(test_image)


# predict
prediction = model.predict_classes(test)

print(prediction)