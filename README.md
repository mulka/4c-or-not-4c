```
      ,---.      _______               ,-----.    .-------.            ,---.   .--.    ,-----.  ,---------.               ,---.      _______    
     /,--.|     /   __  \            .'  .-,  '.  |  _ _   \           |    \  |  |  .'  .-,  '.\          \             /,--.|     /   __  \   
    //_  ||    | ,_/  \__)          / ,-.|  \ _ \ | ( ' )  |           |  ,  \ |  | / ,-.|  \ _ \`--.  ,---'            //_  ||    | ,_/  \__)  
   /_( )_||  ,-./  )               ;  \  '_ /  | :|(_ o _) /           |  |\_ \|  |;  \  '_ /  | :  |   \              /_( )_||  ,-./  )        
  /(_ o _)|  \  '_ '`)             |  _`,/ \ _/  || (_,_).' __         |  _( )_\  ||  _`,/ \ _/  |  :_ _:             /(_ o _)|  \  '_ '`)      
 / /(_,_)||_  > (_)  )  __         : (  '\_/ \   ;|  |\ \  |  |        | (_ o _)  |: (  '\_/ \   ;  (_I_)            / /(_,_)||_  > (_)  )  __  
/  `-----' ||(  .  .-'_/  )         \ `"/  \  ) / |  | \ `'   /        |  (_,_)\  | \ `"/  \  ) /  (_(=)_)          /  `-----' ||(  .  .-'_/  ) 
`-------|||-' `-'`-'     /           '. \_/``".'  |  |  \    /         |  |    |  |  '. \_/``".'    (_I_)           `-------|||-' `-'`-'     /  
        '-'     `._____.'              '-----'    ''-'   `'-'          '--'    '--'    '-----'      '---'                   '-'     `._____.'   
                                                                                                                                                
```

A question that I asked myself as I was styling my hair. _Can I train a computer to identify what is 4C hair and what is not 4C hair?_ With this question alone it sparked a 15 hour hack on understanding the basics of Machine Learning. The goal of this project is to use an already existing model and train it to do following:
- Define the face
- Define the hair
- Define the hair type

**DISCLAIMER**: This project is my first introduction to Machine Learning concepts.

## Getting Started
### Dependencies
This project uses `pip` as a package manager.
- google_images_download 
- imutils
- imageio
- keras
- numpy
- pandas
- sklearn
- tqdm

### Installation
* To install project dependencies run `pip install -r requirements.txt`
#### Download the Images
The `google-search.py` file is bit hard-coded in certain places so to download the images that you will have edit line 4 and interchange between "2c hair" and "4c black hair".
1. Run `python goole-search.py` with an edit to line 4 `keywords = "4c black hair"`
2. Create a `4cblackhair` directory within `rawimages/`
3. Repeat steps 1-2 but replace the keyword with `2chair` and directory.

### Formatting Images
There may be some corrupted images from the download, so for the time being manually delete those images.
1. Change line 9-10 and 22 to the your file path of your cloned repo with "4cblackhair" appended at the end of it.
2. Run `python clean-data.py`.
3. Repeat steps 1-2 but change the last part of the file path to "2chair".

### Train Model
To train the model and view the output just run `python build-model.py`. This will list out the results but those results are **broken** and **inaccurate**.
 
## System Design
Coming Soon!

## Testing
Coming Soon!

## Roadmap
-[ ] 

## Contributing

## Troubleshooting
Coming Soon!
## References
Here are some resources that I was using to first start the project:
* [Transfer Learning with Keras and Deep Learning](https://www.pyimagesearch.com/2019/05/20/transfer-learning-with-keras-and-deep-learning/)
* [Build Image Classification Model in 10 Minutes]( https://www.analyticsvidhya.com/blog/2019/01/build-image-classification-model-10-minutes/)